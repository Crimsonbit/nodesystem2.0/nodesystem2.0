package at.crimsonbit.nodesystem2.gui.drawables.node;

import at.crimsonbit.nodesystem2.backend.AbstractNode;

/**
 * <h1>GUINode</h1> <br>
 * Default implementation of a node in the NodeSystem.
 * 
 * @author Florian Wagner
 * @see AbstractNode
 * @see GUINodeBase
 */
public class GUINode extends GUINodeBase {

	/**
	 * Creates a new GUINode. <br>
	 * Superconstructor from {@link GUINodeBase}.
	 * 
	 * @param node
	 *            associated backend node
	 */
	public GUINode(AbstractNode node) {
		super(node);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AbstractNode getNode() {
		return backendNode;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void drawNode() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void drawInputPorts() {

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void drawOutputPorts() {

	}
}