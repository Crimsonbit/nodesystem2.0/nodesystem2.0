package at.crimsonbit.nodesystem2.gui.drawables.node;

import at.crimsonbit.nodesystem2.backend.AbstractNode;
import at.crimsonbit.nodesystem2.state.IStateMachine;
import at.crimsonbit.nodesystem2.state.State;

/**
 * <h1>GUINodeBase</h1> <br>
 * Base class of every GUI related node. Implements {@link IGUIDrawableNode} and
 * {@link IStateMachine}.
 * 
 * @author Florian Wagner
 * @see IGUIDrawableNode
 * @see IStateMachine
 */
public abstract class GUINodeBase implements IGUIDrawableNode, IStateMachine {

	protected AbstractNode backendNode; // associated node
	protected State currentState; // the current state of the node
	protected State idleState; // the default (idle) state of the node

	/**
	 * Creates a new GUINode. <br>
	 * 
	 * 
	 * @param node
	 *            associated backend node
	 */
	public GUINodeBase(AbstractNode node) {
		this.backendNode = node;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getCurrentState() {
		return currentState;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getIdleState() {
		return idleState;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		setState(idleState);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setState(State state) {
		this.currentState = state;
	}

}
