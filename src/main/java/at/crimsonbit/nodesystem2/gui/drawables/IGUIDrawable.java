package at.crimsonbit.nodesystem2.gui.drawables;

/**
 * <h1>IGUIDrawable</h1> <br>
 * The {@link IGUIDrawable} interface is the parent interface for everything
 * that can/should be drawn in the NodeSystem.
 * 
 * @author Florian Wagner
 *
 */
public interface IGUIDrawable {

	/**
	 * This method is called when the layout is being generated / when the layout is
	 * needed by the NodeSystem.
	 */
	void GUICreateLayout();

}
