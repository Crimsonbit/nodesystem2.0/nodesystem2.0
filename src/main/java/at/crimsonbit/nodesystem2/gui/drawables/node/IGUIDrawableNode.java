package at.crimsonbit.nodesystem2.gui.drawables.node;

import at.crimsonbit.nodesystem2.backend.AbstractNode;
import at.crimsonbit.nodesystem2.gui.drawables.IGUIDrawable;

/**
 * <h1>IGUIDrawableNode</h1> <br>
 * Extension of {@link IGUIDrawable} for use as nodes.
 * 
 * @author Florian Wagner
 * @see IGUIDrawable
 */
public interface IGUIDrawableNode extends IGUIDrawable {

	/**
	 * @return the corresponding backend node of this GUINode
	 */
	AbstractNode getNode();

	/**
	 * This method is responsible for drawing the node background / the node itself.
	 */
	void drawNode();

	/**
	 * This method is responisble for drawing all input ports of the node.
	 */
	void drawInputPorts();

	/**
	 * This method is responsible for drawing all output ports of the node.
	 */
	void drawOutputPorts();

	/**
	 * Default Implementation for GUINodes.<br>
	 * <br>
	 * {@inheritDoc}
	 */
	@Override
	default void GUICreateLayout() {
		drawNode();
		drawOutputPorts();
		drawInputPorts();
	}

}