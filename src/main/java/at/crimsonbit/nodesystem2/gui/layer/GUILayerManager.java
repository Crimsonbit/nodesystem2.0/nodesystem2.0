package at.crimsonbit.nodesystem2.gui.layer;

import javafx.scene.layout.StackPane;

/**
 * <h1>GUILayerManager</h1> <br>
 * 
 * @author Florian Wagner
 *
 */
public class GUILayerManager implements IGUILayerManager {

	private final StackPane guiLayers;

	public GUILayerManager() {
		this.guiLayers = new StackPane();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StackPane getGuiLayers() {
		return guiLayers;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void addLayer(GUILayer layer) {
		guiLayers.getChildren().add(layer);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public GUILayer getLayer(int index) {
		GUILayer layer = null;
		try {
			layer = (GUILayer) guiLayers.getChildren().get(index);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Error cought in the GUILayerManager! Index out of bounds!");
			e.printStackTrace();
		}
		return layer;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeLayer(int index) {
		try {
			guiLayers.getChildren().remove(index);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Error cought in the GUILayerManager! Index out of bounds!");
			e.printStackTrace();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void removeLayer(GUILayer layer) {
		try {
			guiLayers.getChildren().remove(layer);
		} catch (IndexOutOfBoundsException e) {
			System.err.println("Error cought in the GUILayerManager! Index out of bounds!");
			e.printStackTrace();
		}
	}

}
