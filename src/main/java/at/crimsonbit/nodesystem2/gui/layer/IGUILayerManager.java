package at.crimsonbit.nodesystem2.gui.layer;

import javafx.scene.layout.StackPane;

/**
 * <h1>IGUILayerManager</h1> <br>
 * The {@link IGUILayerManager} handles the layers of the NodeSystem.
 * 
 * 
 * @author Florian Wagner
 *
 */
public interface IGUILayerManager {

	/**
	 * Returns all {@link GUILayer} objects as StackPane.
	 * 
	 * @return all layers
	 */
	public StackPane getGuiLayers();

	/**
	 * Adds a new {@link GUILayer} object to the layers. Keep in mind that the layer
	 * which got added last, will be drawn ontop of the previous layers.
	 * 
	 * @param layer
	 *            the GUILayer to add
	 */
	public void addLayer(GUILayer layer);

	/**
	 * Retrieves a layer this the corresponding index or null
	 * 
	 * @param index
	 *            the index of the layer
	 * @return the layer or null
	 */
	public GUILayer getLayer(int index);

	/**
	 * Removes the layer with the given index from the layer stack.
	 * 
	 * @param index
	 *            the index of the layer
	 */
	public void removeLayer(int index);

	/**
	 * Removes the given layer from the layer stack
	 * 
	 * @param layer
	 *            the layer to remove
	 */
	public void removeLayer(GUILayer layer);

}
