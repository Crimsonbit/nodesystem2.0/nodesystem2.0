package at.crimsonbit.nodesystem2.gui.layer;

import javafx.scene.layout.AnchorPane;

/**
 * <h1>GUILayer</h1> <br>
 * The {@link GUILayer} represents a single layer in the NodeSystem. A GUILayer
 * can hold for example the background or the nodes. They are all manage by the
 * GUILayerManager <br>
 * The GUILayer extends {@link AnchorPane}.
 * 
 * @author Florian Wagner
 *
 */
public class GUILayer extends AnchorPane {

}
