package at.crimsonbit.nodesystem2;

import com.google.common.eventbus.EventBus;

/**
 * <h1>Globals</h1><br>
 * Class to store all global variables and containers aswell as the global
 * {@link EventBus}.
 * 
 * @author Florian Wagner
 *
 */
public class Globals {

	private static EventBus GLOBAL_EVENT_BUS;

	/**
	 * This method returns the global {@link EventBus} of the NodeSystem. <br>
	 * The bus is used throughout the whole NodeSystem. It is important to make
	 * sure, that there are no loops created, as it will slow the whole program
	 * down.
	 * 
	 * @return the global {@link EventBus}
	 */
	public EventBus getGlobalEventBus() {
		if (GLOBAL_EVENT_BUS == null)
			GLOBAL_EVENT_BUS = new EventBus();
		return GLOBAL_EVENT_BUS;
	}

}
