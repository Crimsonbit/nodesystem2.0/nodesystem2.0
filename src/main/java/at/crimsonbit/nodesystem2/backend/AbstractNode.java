package at.crimsonbit.nodesystem2.backend;

import java.util.Optional;

import com.google.common.collect.ImmutableCollection;

import at.crimsonbit.nodesystem2.backend.annotation.InputPort;
import at.crimsonbit.nodesystem2.backend.annotation.OutputPort;
import at.crimsonbit.nodesystem2.backend.except.NodeComputationException;
import at.crimsonbit.nodesystem2.backend.port.PortInput;
import at.crimsonbit.nodesystem2.backend.port.PortOutput;

/**
 * A Class representing a Node, Ports are Variables annotated with
 * {@link InputPort} or {@link OutputPort}
 */
public abstract class AbstractNode {

	private boolean isComputed = false;
	/**
	 * The id of the Node, this is a unique identifier in the graph this node is in
	 */
	public final String id;

	/**
	 * Constructs a new Node with given id
	 * 
	 * @param id
	 */
	public AbstractNode(String id) {
		this.id = id;
	}

	/**
	 * Internale compute Method
	 * 
	 * @throws NodeComputationException
	 *             when an exception is thrown in the compute Method or it was
	 *             unsuccessful.
	 */
	final void doCompute() throws NodeComputationException {
		try {
			if (compute())
				isComputed = true;
			else
				throw new NodeComputationException("Computation was not successful", this);
		} catch (Throwable cause) {
			if (cause.getClass() == NodeComputationException.class)
				throw cause;
			throw new NodeComputationException(cause, this);
		}
	}

	/**
	 * Mark this node as modified, is called when a Node in its Path is changed or
	 * added to mark it to be computed again
	 */
	final void markModified() {
		isComputed = false;
	}

	/**
	 * Returns true if this Node has been computed and the values has not been
	 * marked old by {@link AbstractNode#markModified()}. Can be used to only
	 * recalculate required nodes on a change in the graph
	 * 
	 * @return
	 */
	public boolean isComputed() {
		return isComputed;
	}

	/**
	 * A Method for the Node to compute itself. It is called after all InputPorts
	 * have values assigned to them, so they can be used.
	 * 
	 * @throws NodeComputationException
	 *             if the computations failed and there should be a detailed message
	 * 
	 * @return if the computation was successful or a compute exception should be
	 *         raised
	 */
	public abstract boolean compute() throws NodeComputationException;

	/**
	 * Gets a Port by name, this returns either an output or input Port
	 * 
	 * @param portName
	 * @return
	 */
	public Optional<? extends IPort> getPortByName(String portName) {
		Optional<? extends IPort> in = getInputByName(portName);
		if (in.isPresent())
			return in;
		return getOutputByName(portName);
	}

	/**
	 * Gets an InputPort by name.
	 * 
	 * @param portName
	 * @return
	 */
	public abstract Optional<PortInput> getInputByName(String portName);

	/**
	 * Gets an OutputPort by name
	 * 
	 * @param portName
	 * @return
	 */
	public abstract Optional<PortOutput> getOutputByName(String portName);

	/**
	 * Gets a Collection of all Inputs of this Node, the collection should not be
	 * stored forever because it prevents garbage collection.
	 * 
	 * @return
	 */
	public abstract ImmutableCollection<PortInput> getAllInputs();

	/**
	 * Gets a Collection of all Outputs of this Node, the collection should not be
	 * stored forever because it prevents garbage collection.
	 * 
	 * @return
	 */
	public abstract ImmutableCollection<PortOutput> getAllOutputs();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AbstractNode other = (AbstractNode) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

}
