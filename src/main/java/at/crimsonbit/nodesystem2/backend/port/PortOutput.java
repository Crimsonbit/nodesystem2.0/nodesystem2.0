package at.crimsonbit.nodesystem2.backend.port;

import at.crimsonbit.nodesystem2.backend.IPort;

/**
 * Base Class for Output Ports, extends {@link IPort}
 * 
 * @author Alexander Daum
 *
 */
public abstract class PortOutput implements IPort {

}
