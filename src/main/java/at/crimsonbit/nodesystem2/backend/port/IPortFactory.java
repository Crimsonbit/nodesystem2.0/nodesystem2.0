package at.crimsonbit.nodesystem2.backend.port;

import at.crimsonbit.nodesystem2.backend.IPort;

/**
 * Base Interface for Port Factories
 * 
 * @author Alexander Daum
 *
 * @param <T>
 *            The Type of Port
 */
public interface IPortFactory<T extends IPort> {
	/**
	 * Create a new Port with specified Object
	 * 
	 * @param holder
	 * @return
	 */
	T create(Object holder);
}
