package at.crimsonbit.nodesystem2.backend.port;

import java.lang.reflect.Field;
import java.util.Optional;

import com.google.common.base.Function;

import at.crimsonbit.nodesystem2.backend.except.PortRuntimeException;
import at.crimsonbit.nodesystem2.backend.util.BackendUtils;

public class PortOutputReflection extends PortOutput {

	private final Object holder;
	private final Field field;

	public PortOutputReflection(Object holder, Field field) {
		super();
		this.holder = holder;
		this.field = field;
	}

	@Override
	public Optional<Object> get() {
		try {
			return Optional.ofNullable(field.get(holder));
		} catch (Throwable e) {
			throw new PortRuntimeException("Exception occured while getting Value", e, this);
		}

	}

	@Override
	public void set(Object o) {
		try {
			field.set(holder, o);
		} catch (Throwable e) {
			throw new PortRuntimeException("Exception occured while getting Value", e, this);
		}
	}

	public static class Factory implements IPortFactory<PortOutputReflection> {
		private final Field field;

		public Factory(Field f) {
			this.field = f;
		}

		@Override
		public PortOutputReflection create(Object holder) {
			return new PortOutputReflection(holder, field);
		}

	}

}
