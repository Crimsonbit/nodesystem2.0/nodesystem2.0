package at.crimsonbit.nodesystem2.backend.port;

public interface SupplierThrows<T> {
	T get() throws Exception;
}
