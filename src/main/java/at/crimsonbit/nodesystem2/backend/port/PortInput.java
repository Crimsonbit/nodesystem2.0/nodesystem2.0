package at.crimsonbit.nodesystem2.backend.port;

import java.util.Optional;

import at.crimsonbit.nodesystem2.backend.IPort;

/**
 * Base Class for Input Ports, extends {@link IPort}
 * 
 * @author Alexander Daum
 *
 */
public abstract class PortInput implements IPort {
	Optional<PortOutput> connection = Optional.empty();

	/**
	 * Returns the {@link PortOutput} to which this Port is connected, if this Port
	 * is not connected, this returns an empty {@link Optional}
	 * 
	 * @return the connected Port or an empty Optional if not connected
	 */
	public Optional<PortOutput> getConnection() {
		return connection;
	}

	/**
	 * Connects this Port to a {@link PortOutput}. To disconnect this, use
	 * {@link PortInput#disconnect()}
	 * 
	 * @throws NullPointerException
	 *             if con is null
	 * @param con
	 *            the {@link PortOutput} to connect to, may not be null
	 */
	public void connect(PortOutput con) {
		connection = Optional.of(con);
	}

	/**
	 * Removes a The connection to this Port.
	 */
	public void disconnect() {
		connection = Optional.empty();
	}
}
