package at.crimsonbit.nodesystem2.backend.port;

import java.lang.reflect.Field;
import java.util.Optional;

import at.crimsonbit.nodesystem2.backend.except.PortRuntimeException;

public class PortInputReflection extends PortInput {
	private final Object holder;
	private final Field field;

	public PortInputReflection(Object holder, Field field) {
		super();
		this.holder = holder;
		this.field = field;
	}

	@Override
	public Optional<Object> get() {
		try {
			return Optional.ofNullable(field.get(holder));
		} catch (Throwable e) {
			throw new PortRuntimeException("Exception occured while getting Value", e, this);
		}
	}

	@Override
	public void set(Object o) {
		try {
			field.set(holder, o);
		} catch (Throwable e) {
			throw new PortRuntimeException("Exception occured while getting Value", e, this);
		}
	}

	public static class Factory implements IPortFactory<PortInputReflection> {
		private final Field field;

		public Factory(Field field) {
			this.field = field;
		}

		public PortInputReflection create(Object holder) {
			return new PortInputReflection(holder, field);
		}
	}

}
