package at.crimsonbit.nodesystem2.backend.node;

/**
 * An Interface for creating Node Types. A NodeType is any Class implementing
 * this interface, it can also be an enum. <br>
 * Implementations of this class should also override hashCode and equals as
 * this is used as a key in a HashMap
 * 
 * @author Alexander Daum
 *
 */
public interface INodeType {
	/**
	 * Return a unique name for the Node Type
	 * 
	 * @return
	 */
	String regname();

	/**
	 * Return a name to display the NodeType, this does not have to be unique, but
	 * should describe the node type in a good human readable form. By default this
	 * returns the {@link INodeType#regname()}
	 * 
	 * @return
	 */
	default String displayname() {
		return regname();
	}

	@Override
	int hashCode();

	@Override
	boolean equals(Object obj);
}
