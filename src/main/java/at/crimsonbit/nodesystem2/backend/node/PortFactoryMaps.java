package at.crimsonbit.nodesystem2.backend.node;

import java.util.Map;

import at.crimsonbit.nodesystem2.backend.port.IPortFactory;
import at.crimsonbit.nodesystem2.backend.port.PortInput;
import at.crimsonbit.nodesystem2.backend.port.PortOutput;

class PortFactoryMaps {
	public final Map<String, IPortFactory<? extends PortInput>> ins;

	public final Map<String, IPortFactory<? extends PortOutput>> outs;

	public PortFactoryMaps(Map<String, IPortFactory<? extends PortInput>> ins,
			Map<String, IPortFactory<? extends PortOutput>> outs) {
		super();
		this.ins = ins;
		this.outs = outs;
	}
	
	
}