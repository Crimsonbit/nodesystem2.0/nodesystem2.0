package at.crimsonbit.nodesystem2.backend.node;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;

import at.crimsonbit.nodesystem2.backend.AbstractNode;
import at.crimsonbit.nodesystem2.backend.annotation.InputPort;
import at.crimsonbit.nodesystem2.backend.annotation.OutputPort;
import at.crimsonbit.nodesystem2.backend.port.IPortFactory;
import at.crimsonbit.nodesystem2.backend.port.PortInput;
import at.crimsonbit.nodesystem2.backend.port.PortInputReflection;
import at.crimsonbit.nodesystem2.backend.port.PortOutput;
import at.crimsonbit.nodesystem2.backend.port.PortOutputReflection;
import at.crimsonbit.nodesystem2.backend.util.AnnotationUtils;

/**
 * An Implementation of {@link AbstractNode} for Node Classes, that use
 * Annotated Fields for the Node Ports. They are added automatically in the
 * constructor
 * 
 * @author Alexander Daum
 *
 */
public abstract class NodeAnnotated extends AbstractNode {

	private static LoadingCache<Class<?>, PortFactoryMaps> portMapCache = CacheBuilder.newBuilder().maximumSize(256)
			.expireAfterWrite(10, TimeUnit.MINUTES).build(new CacheLoader<Class<?>, PortFactoryMaps>() {
				@Override
				public PortFactoryMaps load(Class<?> key) {
					return calculatePortMaps(key);
				}
			});

	private static PortFactoryMaps calculatePortMaps(Class<?> clazz) {
		ImmutableMap.Builder<String, IPortFactory<? extends PortInput>> ins = ImmutableMap.builder();
		ImmutableMap.Builder<String, IPortFactory<? extends PortOutput>> outs = ImmutableMap.builder();
		for (Field f : AnnotationUtils.getDeclaredFieldsAnnotatedWith(clazz, NodeAnnotated.class, InputPort.class)) {
			f.setAccessible(true);
			ins.put(f.getName(), new PortInputReflection.Factory(f));
		}
		for (Field f : AnnotationUtils.getDeclaredFieldsAnnotatedWith(clazz, NodeAnnotated.class, OutputPort.class)) {
			f.setAccessible(true);
			outs.put(f.getName(), new PortOutputReflection.Factory(f));
		}
		return new PortFactoryMaps(ins.build(), outs.build());
	}

	private final ImmutableMap<String, PortInput> inPorts;
	private final ImmutableMap<String, PortOutput> outPorts;

	public NodeAnnotated(String id) {
		super(id);
		PortFactoryMaps facs = portMapCache.getUnchecked(this.getClass());
		inPorts = facs.ins.entrySet().stream()
				.collect(ImmutableMap.toImmutableMap(e -> e.getKey(), e -> e.getValue().create(NodeAnnotated.this)));
		outPorts = facs.outs.entrySet().stream()
				.collect(ImmutableMap.toImmutableMap(e -> e.getKey(), e -> e.getValue().create(NodeAnnotated.this)));
	}

	@Override
	public Optional<PortInput> getInputByName(String portName) {
		return Optional.ofNullable(inPorts.get(portName));
	}

	@Override
	public Optional<PortOutput> getOutputByName(String portName) {
		return Optional.ofNullable(outPorts.get(portName));
	}

	@Override
	public ImmutableCollection<PortInput> getAllInputs() {
		return inPorts.values();
	}

	@Override
	public ImmutableCollection<PortOutput> getAllOutputs() {
		return outPorts.values();
	}

}
