package at.crimsonbit.nodesystem2.backend;

import java.util.Optional;

import com.google.common.base.Supplier;

import at.crimsonbit.nodesystem2.backend.except.PortRuntimeException;
import at.crimsonbit.nodesystem2.backend.port.SupplierThrows;

/**
 * Base Interface for all NodePorts
 * 
 * @author Alexander Daum
 *
 */
public interface IPort {
	/**
	 * Gets the Value of this Port and returns it. If this Port has no Value, it
	 * returns an empty Optional.
	 * 
	 * @return
	 */
	Optional<Object> get();

	void set(Object o);

}
