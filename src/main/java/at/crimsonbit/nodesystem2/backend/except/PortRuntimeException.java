package at.crimsonbit.nodesystem2.backend.except;

import at.crimsonbit.nodesystem2.backend.IPort;

public class PortRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 844630836725089872L;
	public final IPort port;

	public PortRuntimeException(String message, Throwable cause, IPort port) {
		super(message, cause);
		this.port = port;
	}

}
