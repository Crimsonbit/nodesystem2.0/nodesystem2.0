package at.crimsonbit.nodesystem2.backend.except;

import at.crimsonbit.nodesystem2.backend.AbstractNode;

public class NodeComputationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7724129039221168807L;
	public final AbstractNode node;

	public NodeComputationException(String message, AbstractNode node) {
		super(message);
		this.node = node;
	}

	public NodeComputationException(Throwable cause, AbstractNode node) {
		super(cause);
		this.node = node;
	}

}
