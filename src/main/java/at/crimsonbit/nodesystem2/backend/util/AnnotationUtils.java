package at.crimsonbit.nodesystem2.backend.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class AnnotationUtils {
	public static List<Field> getDeclaredFieldsAnnotatedWith(Class<?> clazz, Class<? extends Annotation> annotation) {
		return getDeclaredFieldsAnnotatedWith(clazz, clazz.getSuperclass(), annotation);
	}

	public static List<Field> getDeclaredFieldsAnnotatedWith(Class<?> baseClass, Class<?> upTo,
			Class<? extends Annotation> annotation) {
		return getDeclaredFieldsAnnotatedWith(baseClass, upTo, annotation, new ArrayList<>());
	}

	private static List<Field> getDeclaredFieldsAnnotatedWith(Class<?> clazz, Class<?> upTo,
			Class<? extends Annotation> annotation, List<Field> l) {
		if (clazz == upTo)
			return l;
		for (Field f : clazz.getDeclaredFields()) {
			if (f.isAnnotationPresent(annotation))
				l.add(f);
		}
		return getDeclaredFieldsAnnotatedWith(clazz.getSuperclass(), upTo, annotation, l);
	}

}
