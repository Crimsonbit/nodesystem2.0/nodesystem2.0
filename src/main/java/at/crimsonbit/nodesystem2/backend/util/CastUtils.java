package at.crimsonbit.nodesystem2.backend.util;

import java.util.Optional;

import com.google.common.primitives.Primitives;

public class CastUtils {
	/**
	 * Checks if a conversion from O to T is possible and if it is, the conversion
	 * is performed.
	 * 
	 * @param <T>
	 *            The Target Type
	 * @param <O>
	 *            The Original Type
	 * @param o
	 *            An Object that should be converted
	 * @param targetClazz
	 *            the target class (is needed, because Generics contain no type
	 *            information and are erased at runtime)
	 * @return An Optional holding an Object of Type targetClazz if the cast is
	 *         possbile, Optional.empty() if not or o is null
	 */
	public static <T, O> Optional<T> cast(O o, Class<T> t) {
		if (o == null)
			return Optional.empty();
		if (t.isInstance(o))
			return Optional.of(t.cast(o));
		if (t.isPrimitive())
			return castPrimitive(o, t);
		return Optional.empty();
	}

	@SuppressWarnings("unchecked")
	private static <T, O> Optional<T> castPrimitive(O o, Class<T> t) {
		if (Primitives.unwrap(o.getClass()) == t) {
			// o is a wrapper of t, can be cast
			return (Optional<T>) Optional.of(o);
		}
		if (o instanceof Number) {
			// All wrappers of number types and also other numbers are instance of number
			Number n = (Number) o;
			// Target should be a Number
			if (t == byte.class)
				return (Optional<T>) Optional.of(n.byteValue());
			if (t == short.class)
				return (Optional<T>) Optional.of(n.shortValue());
			if (t == int.class)
				return (Optional<T>) Optional.of(n.intValue());
			if (t == long.class)
				return (Optional<T>) Optional.of(n.longValue());
			if (t == float.class)
				return (Optional<T>) Optional.of(n.floatValue());
			if (t == double.class)
				return (Optional<T>) Optional.of(n.doubleValue());
		}
		// o is no number and not of a wrapper class, so not able to cast
		return Optional.empty();
	}
}
