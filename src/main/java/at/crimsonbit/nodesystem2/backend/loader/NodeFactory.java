package at.crimsonbit.nodesystem2.backend.loader;

import at.crimsonbit.nodesystem2.backend.AbstractNode;

@FunctionalInterface
public interface NodeFactory {
	AbstractNode create(String id);
}
