package at.crimsonbit.nodesystem2.backend.loader;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

import at.crimsonbit.nodesystem2.backend.AbstractNode;
import at.crimsonbit.nodesystem2.backend.node.INodeType;

public class NodeRegistry {

	private Map<INodeType, NodeFactory> factories = new HashMap<>();

	public AbstractNode instantiate(INodeType type, String id) {
		return null;
	}

	protected ModuleRegister getNewRegister() {
		return new ModuleRegister(this);
	}

	protected void registerNodeFactory(INodeType type, NodeFactory fac) {
		Objects.requireNonNull(type);
		Objects.requireNonNull(fac);
		if (factories.containsKey(type)) {
			throw new RuntimeException("Node Type " + type.regname() + " has already been registered");
		}
		factories.put(type, fac);
	}
}
