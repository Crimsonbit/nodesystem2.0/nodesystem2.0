package at.crimsonbit.nodesystem2.backend.loader;

import java.util.function.Function;

import at.crimsonbit.nodesystem2.backend.AbstractNode;
import at.crimsonbit.nodesystem2.backend.node.INodeType;

/*
 * A Class for registering Node Types
 */
public class ModuleRegister {

	private final NodeRegistry reg;

	public ModuleRegister(NodeRegistry reg) {
		super();
		this.reg = reg;
	}

	/**
	 * Register a Node Factory to create a new Node with a given ID. Each factory is
	 * linked to an INodeType. <br>
	 * The factory is a functional interface and can be supplied as a lambda
	 * expression or method reference (even a Method reference to a Constructor)
	 * 
	 * @param factory
	 */
	public void registerNode(INodeType type, NodeFactory factory) {
		
	}

}
