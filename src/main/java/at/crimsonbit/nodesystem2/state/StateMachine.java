package at.crimsonbit.nodesystem2.state;

/**
 * <h1>StateMachine</h1> <br>
 * Implementation of {@link IStateMachine}. <br>
 * The default state machine of the NodeSystem.
 * 
 * @author Florian Wagner
 *
 */
public class StateMachine implements IStateMachine {

	private State currentState;
	private State idleState;

	public StateMachine(State idleState) {
		this.idleState = idleState;
		reset();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getCurrentState() {
		return currentState;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public State getIdleState() {
		return idleState;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void reset() {
		this.currentState = this.idleState;
	}

	@Override
	public void setState(State state) {
		this.currentState = state;
	}

}
