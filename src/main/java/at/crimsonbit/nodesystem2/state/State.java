package at.crimsonbit.nodesystem2.state;

import java.io.Serializable;

/**
 * <h1>State</h1> <br>
 * This class represents a state that the NodeSystem can be in.
 * 
 * @author Florian Wagner
 *
 */
public class State implements Serializable {

	private static final long serialVersionUID = -6469729026173646343L;
	
	private String name;

	/**
	 * Create a new {@link State}.
	 *
	 * @param name
	 *            the name of the state
	 */
	public State(final String name) {
		this.name = name;
	}

	/**
	 * Get state name.
	 * 
	 * @return state name
	 */
	public String getName() {
		return name;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		State state = (State) o;

		return name.equals(state.name);

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		return name.hashCode();
	}

}