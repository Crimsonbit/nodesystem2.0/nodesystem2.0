package at.crimsonbit.nodesystem2.state;

import java.util.Set;

/**
 * <h1>IStateMachine</h1> <br>
 * Base interface of any state machine in the NodeSystem.
 * 
 * @author Florian Wagner
 *
 */
public interface IStateMachine {

	/**
	 * Returns the current state the NodeSystem is in.
	 * 
	 * @return the current state
	 */
	State getCurrentState();

	/**
	 * This method returns the idle state that the NodeSystem should have.
	 * 
	 * @return the idle state
	 */
	State getIdleState();

	/**
	 * Resets the state machine to its idle state.
	 */
	void reset();

	/**
	 * Sets the current state of the state machine.
	 * 
	 * @param state
	 *            the state to set the state machine to
	 */
	void setState(State state);

}
