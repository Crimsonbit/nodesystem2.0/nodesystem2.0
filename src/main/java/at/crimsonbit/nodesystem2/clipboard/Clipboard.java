package at.crimsonbit.nodesystem2.clipboard;

import java.io.Serializable;

import org.apache.commons.lang.SerializationUtils;

/**
 * <h1>Clipboard</h1> <br>
 * Default Clipboard implementation of {@link IClipboard}.
 * 
 * @author Florian Wagner
 *
 * @param <T>
 *            the type of the object to copy
 */
@SuppressWarnings("unchecked")
public class Clipboard<T extends Serializable> implements IClipboard<T> {

	private T object;

	/**
	 * {@inheritDoc}
	 */

	@Override
	public T copy(T obj) {
		return (T) SerializationUtils.clone(obj);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public T paste() {
		return this.object;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void copyAndHold(T obj) {
		this.object = (T) SerializationUtils.clone(obj);

	}

}
