package at.crimsonbit.nodesystem2.clipboard;

import java.io.Serializable;

/**
 * <h1>IClipboard</h1> <br>
 * The {@link IClipboard} interface.
 * 
 * @author Florian Wagner
 *
 * @param <T>
 *            the object to copy. This object must implement
 *            {@link Serializable}.
 */
public interface IClipboard<T extends Serializable> {

	/**
	 * Copies the given object and stores it inside this clipboard. The copied
	 * object van be retrieved with {@link #paste()}.
	 * 
	 * @param obj
	 *            the object to copy
	 */
	void copyAndHold(T obj);

	/**
	 * Copies the given object and returns it directly. <br>
	 * Note: This method does <b> NOT </b> override the internally stored object!
	 * <br>
	 * It will return a deep copy assertEquals() -> true!
	 * 
	 * @param obj
	 *            the object to clone
	 * @return the cloned object
	 */
	T copy(T obj);

	/**
	 * Returns the internally stored object which has been copied with
	 * {@link #copyAndHold(Serializable)} or null if no object is stored.<br>
	 * It will return a deep copy assertEquals() -> true!
	 * 
	 * @return the copied object or null
	 */
	T paste();

}
