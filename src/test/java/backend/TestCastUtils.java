package backend;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem2.backend.util.CastUtils;

class TestCastUtils {

	@Test
	void testPrimitiveCast() {
		Integer i = 3;
		BigDecimal bd = BigDecimal.TEN;
		// Have to check with Wrappers here, because
		assertEquals(Byte.valueOf(bd.byteValue()), CastUtils.cast(bd, byte.class).get());
		assertEquals(Short.valueOf(bd.shortValue()), CastUtils.cast(bd, short.class).get());
		assertEquals(Integer.valueOf(i.intValue()), CastUtils.cast(i, int.class).get());
		assertEquals(Integer.valueOf(bd.intValue()), CastUtils.cast(bd, int.class).get()); // use i and bd for int
		assertEquals(Long.valueOf(bd.longValue()), CastUtils.cast(bd, long.class).get());
		assertEquals(Float.valueOf(bd.floatValue()), CastUtils.cast(bd, float.class).get());
		assertEquals(Double.valueOf(bd.doubleValue()), CastUtils.cast(bd, double.class).get());

		assertEquals(Optional.empty(), CastUtils.cast(bd, char.class));

		assertEquals(Optional.empty(), CastUtils.cast("10", int.class));
	}

	@Test
	void testNull() {
		assertEquals(Optional.empty(), CastUtils.cast(null, Object.class));
	}

	@Test
	void Superclass() {
		ArrayList<Integer> al = new ArrayList<>();
		al.add(10);

		assertEquals(10, CastUtils.cast(al, List.class).get().get(0));
	}

	@Test
	void testUnfitting() {
		assertEquals(Optional.empty(), CastUtils.cast("10", BigDecimal.class));
	}

	@Test
	void testConstructor() {
		new CastUtils(); // FOR THE COVERAGE
	}

}
