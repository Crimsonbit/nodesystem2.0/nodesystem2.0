package msc;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem2.clipboard.Clipboard;
import at.crimsonbit.nodesystem2.state.State;

class TestClipboard {

	Clipboard<State> clipboard;
	State state;

	@BeforeEach
	void setUp() throws Exception {
		state = new State("Test State");
		clipboard = new Clipboard<State>();
		assertNotNull(state);
		assertNotNull(clipboard);
		
	}

	@Test
	void testCopy() {
		assertEquals(state, clipboard.copy(state));
	}

	@Test
	void testPaste() {
		clipboard.copyAndHold(state);
		assertEquals(state, clipboard.paste());
	}

}
