package msc;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem2.state.State;
import at.crimsonbit.nodesystem2.state.StateMachine;

class TestStateMachine {

	private StateMachine fsm;
	private State s1;
	private State s2;
	private State idle;

	@Test
	void setUp() throws Exception {

		s1 = new State("S1");
		s2 = new State("S2");
		idle = new State("Idle");

		fsm = new StateMachine(idle);

		assertNotNull(s1);
		assertNotNull(s2);
		assertNotNull(idle);
		assertNotNull(fsm);
		assertEquals(idle.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s1.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s2.getName(), fsm.getCurrentState().getName());

		fsm.setState(s1);

		assertEquals(s1.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s2.getName(), fsm.getCurrentState().getName());
		assertNotEquals(idle.getName(), fsm.getCurrentState().getName());

		fsm.setState(s2);

		assertEquals(s2.getName(), fsm.getCurrentState().getName());
		assertNotEquals(idle.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s1.getName(), fsm.getCurrentState().getName());

		fsm.reset();

		assertEquals(idle.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s1.getName(), fsm.getCurrentState().getName());
		assertNotEquals(s2.getName(), fsm.getCurrentState().getName());

		assertFalse(s1.equals(s2));

		assertTrue(s1.equals(s1));

		assertFalse(s1.equals(null));
		assertEquals(idle.getName(), fsm.getIdleState().getName());

	}

}
