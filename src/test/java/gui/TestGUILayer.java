package gui;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import at.crimsonbit.nodesystem2.gui.layer.GUILayer;
import at.crimsonbit.nodesystem2.gui.layer.GUILayerManager;
import at.crimsonbit.nodesystem2.gui.layer.IGUILayerManager;

class TestGUILayer {

	private IGUILayerManager layerManager;
	private GUILayer testLayer;

	@BeforeEach
	void setUp() throws Exception {
		layerManager = new GUILayerManager();
		assertNotEquals(null, layerManager);

		testLayer = new GUILayer();
		assertNotEquals(null, testLayer);

		layerManager.addLayer(testLayer);
	}

	@Test
	void testAddLayer() {
		assertEquals(1, layerManager.getGuiLayers().getChildren().size());
	}

	@Test
	void testGetLayers() {
		assertNotEquals(null, layerManager.getGuiLayers());
	}

	@Test
	void testGetLayer() {
		assertNotNull(layerManager.getLayer(0));
	}

	@Test
	void testGetLayerError() {
		assertThrows(IndexOutOfBoundsException.class, () -> layerManager.getLayer(1));
	}

}
